<?php

namespace Drupal\drupal_di\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\drupal_di\GetColors;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a custom Block that sets background color.
 *
 * @Block(
 *   id = "color_block",
 *   admin_label = @Translation("Set Background Color"),
 *   category = @Translation("Set Background Color"),
 * )
 */
class ColorfulBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * Get the List of Colors.
   *
   * @var \Drupal\drupal_di\GetColors
   */
  protected $getColorsService;

  /**
   * Class constructor.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, GetColors $get_colors) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->getColorsService = $get_colors;
  }

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('drupal_di.get_colors')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $config = $this->getConfiguration();
    $build = [
      '#theme' => 'drupal_di_color_block',
      '#result' => [
        'text' => 'Dependency Injection Task',
        'value' => $config['color_value'],
      ],
    ];
    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form = parent::blockForm($form, $form_state);
    $service = $this->getColorsService->getColors();
    $config = $this->getConfiguration();
    // print_r($config['color_value']);exit;.
    $form['color'] = [
      '#type' => 'select',
      '#title' => $this->t('Select Color'),
      '#options' => $service,
      '#default_value' => $config['color_value'],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->setConfigurationValue('color_value', $form_state->getValue('color'));
  }

}
